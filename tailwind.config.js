module.exports = {
  purge: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
   darkMode: false,
   theme: {
     extend: {
      colors: {
        primary: "#517094",
       }
     },
   },
   variants: {
     extend: {},
   },
   plugins: [],
 }